---
title: "Indicateurs complexes"
subtitle: "Statistiques descriptives bivariée"
author: "Elisabeth Morand"
date: "26/04/21 (updated: `r Sys.Date()`)"
output:
  xaringan::moon_reader:
    css: ["default", "assets/theme_diapo.css", "assets/les-fonts.css"]
    seal: false 
    lib_dir: libs
    nature:
      # autoplay: 5000
      highlightStyle: solarized-light
      highlightLanguage: ["r"]
      slideNumberFormat: "" 
      highlightLines: true
      countIncrementalSlides: false
      ratio: "16:9"
---

```{r xaringanExtra, echo=FALSE}
xaringanExtra::use_xaringan_extra(c("tile_view", "animate_css", "Panelset","scribble"))
```

```{r xaringan-panelset, echo=FALSE}
xaringanExtra::use_panelset()
```
```{r, message=FALSE, warning=FALSE,echo=FALSE}

library(TraMineR)
library(icons)


```



```{r broadcast, echo=FALSE}
xaringanExtra::use_broadcast()
```






layout: true

<a class="footer-link" href="https://emorand.gitpages.huma-num.fr/sequence/">site </a>

---

class: title-slide, center, bottom

# `r rmarkdown::metadata$title`

## `r rmarkdown::metadata$subtitle`

### `r rmarkdown::metadata$author` 


???

On s'interesse ici à prendre en compte à la fois les successions et le temps passé dans les états.

---

name: data

## Les données

```{r importation, eval=TRUE,message=FALSE,warning=FALSE,echo=FALSE}
library(here)
petitjeu <- read.delim(here("donnees/in","petitjeu.csv"))
```
```{r telechargement, echo=FALSE,include=knitr::is_html_output()}

library(DT)
datatable(petitjeu, 
  extensions = 'Buttons', options = list(
    dom = 'Bfrtip',
    buttons = 
      list('copy', list(
        extend = 'collection',
        buttons = c('csv', 'excel', 'pdf'),
        text = 'Download'
      ))
  )
)


```
```{r,eval=TRUE,message=FALSE,warning=FALSE,echo=FALSE,results="asis"}
# on reconstruit les sequences
 library(TraMineR)
sequence <- seqdef(petitjeu, var = 2:13)
sequence2<-seqdss(sequence)
library(dplyr)

```
---
background-image: url(images/cercles2.jpg)
background-size: contain
background-position: left
class: middle, left

.pull-right[
## Sous-séquences

1. Sous-séquences version sous-chaines
1. Calcul sous chaines

]

---
# Sous-chaînes vs sous-séquences


.panelset[
.panel[.panel-name[Exemples]
- Exemple x : A-A-B-B-C
	  - Sous-chaîne : ABB est une sous-chaîne de la séquence (les états sont adjacents)
	  - Sous-séquences (Elzinga,2007) : AC est une sous-séquence de x

-Autre exemple y  : A-A-A-A-A-B-B-A-A-A-A-A

- Sous-chaînes : chaîne vide, A, AB, ...
- Sous-séquences : chaîne vide, A, B, ABA, AB, BA, BB

]


.panel[.panel-name[Usages]

Les sous-séquences permettent d'analyser si une succession existe.

Par exemple, par rapport à un état « est marié » si vous testez l’hypothèse « Avoir un enfant arrive après le mariage » , l'analyse des sous-séquences prendra en compte toutes les configurations, pas seulement celles de l'année suivante (si l'unité d'analyse est de un an)



]
]


---
# Sous-chaines

Les sous-chaînes permettent d'avoir une idée du degré de complexité d'une séquence

 Exemple :
 
 - x=ababab a 12 sous-chaînes distinctes  
 - y =abcdef a 22 sous-chaînes distinctes
 
 
La séquence x est moins complexe que y, x est seulement une répétition du même motif ab, (répété 3 fois) 


---
background-image: url(images/deco1.jpg)
background-size: 50% 75%
background-position: left
class: middle, left

.pull-right[
## Indicateurs complexes

1. Une séquence
1. Un ensemble de séquences d'états


]

???

Comme dans le cas des indicateurs simples, il existe des résumés de la séquence 

On peut combiner cette information "indiviuelle" pour obtenir une information résumé sur toutes les séquences

On a aussi des résumés par colonne

---
# Indicateurs complexes
Dans la première partie, nous avons vu comment décrire simplement une séquence d'états : 
nombre d'états, nombre de transitions, longueur de la séquence, temps passé dans chaque état

Comment résumer tous ces indicateurs simples dans une grandeur unique ?

3 points de vue :
  - Entropie
  - Turbulence
  - Complexité


---
# Entropie : Définition

L'entropie longitudinale de Shannon d'une séquence individuelle s'écrit :

$$ h(x)=-\sum_{i=1}^a {p_i log p_i}$$


- $p_i$ est la proportion d'état i dans la séquence individuelle
- a est la longueur de l'alphabet

h(x) est maximale quand les durées passées dans tous les états sont les mêmes (ex : (A,3)-(B,3)-(C,3)-(D,3)) et $h_{max}$= longueur(alphabet) 

En pratique h(x) est standardisé comme (h(x)/hmax) pour varier entre 0 et 1

---
# Exemple

.pull-left[
Exemple : 

A-A-A-A-A-A-A-A-A-A-B-C

par convention : 0 log(0)=0
]

.pull-right[
- $p(A)=\frac{10}{12}log\left(\frac{10}{12}\right)$
- p(B)= $\frac{1}{12}log\left(\frac{1}{12}\right)$
- p(C)= $\frac{1}{12}log\left(\frac{1}{12}\right)$
- p(D)=0
]



$$h(x)=- \left(\frac{10}{12}log\left(\frac{10}{12}\right)+\frac{1}{12}log\left(\frac{1}{12}\right)+\frac{1}{1}log\left(\frac{1}{12}\right)+ log (0) \right)$$

L'entropie pour cette séquence est de `r -(10/12*log(10/12)+2*1/12*log(1/12))`

L'entropie standardisée est  
h(x)/log(4)= `r -(10/12*log(10/12)+2*1/12*log(1/12))/log(4)` 



---
# Entropie : interprétation

L'entropie longitudinale vient de l'entropie classique de Shanon, équivalente à la variance pour les données catégorielles

L'entropie est égale à 0 quand l’individu reste dans le même état durant toute la séquence 

  Dans notre exemple,  (A,12) ou (B,12) sont des exemples de séquences avec une entropie égale à 0
  
L'entropie maximale est atteinte quand la durée passée dans chaque état de l'alphabet est la même

L'entropie est maximale pour la séquence (A,3)-(B,3)-(C,3)-(D,3) mais aussi pour (A,2)-(B,1)-(C,3)-(B,1)-(A,2)-(B,1)-(D,3)

L'entropie est identique pour AABB ou BABA
Seul le temps passé dans chaque état est pris en compte


---
# Turbulence ou complexité (Elzinga et Liefbroer, 2007)

Le premier nom de cet indicateur a été la turbulence mais son créateur a décidé finalement de le nommer complexité

Cet indicateur est un compromis entre la transition et la durée passée dans chaque état. Pour une séquence x, la valeur est obtenue par la formule 

.blockquote[
### `r fontawesome("location-arrow", style = "solid") ` complexité

$$T(x)=\log_2 \{\Phi(x)\frac{s_{t,max} (x)+1}{ s^2_t(x)+1 }\}$$
]

---
##  Elements pour le calcul 

1. $\Phi(x)$ est le nombre de sous-séquences distinctes de la séquence DSS de x

- A-A-A-A-A-B-B-A-A-A-A-A : DSS séquence=ABA
- Sous-séquences : Chaine vide, A,B,ABA,AB,BA,AA ,
- donc  $\Phi(x)$=7

2.$s^2_t$ (x) est la variance des durées dans l'état

3.$ s_{t,max} (x)$ est la valeur maximum de cette variance, étant donnée la durée totale

---
## Un exemple de calcul de la turbulence

Soit la sequence : x=A-A-A-A-A-B-B-A-A-A-A-A

Comme les temps importent, on la réecrit sous le format suivant : x=(A,5)-(B,2)-(A,5) 

1. Il est démontré que si  n nombre d'états distincts dans x et $\bar{t}$ moyenne des durées $t_j$ alors 

$$s_{t,max} (x) = (n-1)(1-\bar{t})^2 $$

1. $ s^2_t $ (x) est la variance des durées dans l'état. Pour l'exemple cette variance vaut 
$$s^2_t (x)=\frac{(5-4)^2+(2-4)^2+(5-4)^2}{3}=2$$
(variance écart à la moyenne, $\bar{t}$, moyenne de 5,2,5)
1. $\Phi(x)=7$
1. $T(x)= log2(7*19/3)= 5.47032$

---
# Indice de complexité ( Gabadinho et al.,2010)
### Indice de complexité ( Gabadinho et al.,2010)

Cet indicateur mixe l'entropie et le nombre de transitions

$$C(x)=\sqrt {\frac{l_d(x)-h(x)}{l(x)-h_{max}}}$$




- h(x) est l'entropie non standardisée 
- hmax=log(a) ( a est la longueur de l'alphabet)
- ld est la longueur de la séquence DSS
- l est la longueur de la séquence

---
# Valeurs de l'indice de complexité 
La valeur minimum de l'indice de complexité est 0, le maximum est 1

Le minimum 0 est obtenu quand la  séquence n'est composée que d'un seul état (entropie=0 et nombre de transitions =0)

Le maximum 1 est obtenu quand les 2  conditions suivantes sont réalisées : 

- Chaque état de l'alphabet est dans la séquence et le temps passé dans chaque état est identique

- Le DSS est égal à la séquence


---

# Indicateurs individuels

```{r,echo=FALSE}
petitjeu$Entropie<-seqient(sequence,norm=FALSE)
 
 petitjeu$Entropie_n<-seqient(sequence)
 
 petitjeu$Turbulence<-seqST(sequence)
 petitjeu$Complexite<-seqici(sequence)
 
  petitjeu%>%
    select(Entropie:Complexite)%>%
  head()
```
---
# Comparaison
```{r,echo=FALSE}
petitjeu%>%
  select("Entropie","Turbulence","Complexite")%>%
  cor()%>%
  round(2)
```
Les mesures donnent sensiblement la même information 
Les coefficients de corrélation entre   mesures sur notre jeu de données sont d'environ 0.8
Cas spécifiques (cf  Gabadinho et al.,2010 ) pour une comparaison entre les 3 indicateurs

---
background-image: url(images/deco1.jpg)
background-size: 50% 100%
background-position: left
class: middle, left

.pull-right[
## Ensemble de sequences

- Usage d'indicateurs individuel
- Distribution transversal
- Entropie



]


???

On décrit un ensemble de séquence

- soit en considerant les séquences comme les modalité d'une variable

- soit en considérant les descripteurs individuels à résumer pour l'ensemble




---
# Utilisation des indicateurs individuels 

- Distribution d'entropie

- Distribution de turbulence 

- Distribution de complexité


---

# Distribution d'états à chaque instant (statistique transversale)

```{r, eval=TRUE,message=FALSE,warning=FALSE,echo=FALSE}
seqstatd(sequence)[[1]]%>%
  round(2)%>%
  as.data.frame()


```

Au temps 0, chacun est dans l'état A 
Au temps 1 une partie des individus est dans l'état B, une autre reste dans l'état A et personne n'est dans l'état C ou D
---
### Entropie transversale de Shanon

L'entropie est utile pour décrire  la distribution des états à chaque instant t

$$E_t=\sum_{j=1}^a \frac{p_{tj}log (p_{tj})}{log(a)}$$

- $p_{tj}$ est la proportion des états j au temps t
et nous  décidons que  0 log(0)= 0 (log est e-base log)
- a: Longueur de l'alphabet
- La valeur minimum est 0: si tous les individus sont dans la même situation au temps t
- La valeur maximale est 1 (valeur théorique maximale  pour l'entropie standardis"e E) si tous les $p_{tj}$  sont égaux au temps t
- La valeur commune est 1/a et la somme égale à log(a)

---

background-image: url(images/deco1.jpg)
background-size: 50% 100%
background-position: left
class: middle, left

.pull-right[
## Visulaisation

- Usage d'indicateurs individuel
- Distribution transversal
- Entropie



]
---

## Graphes de distributions d'états

```{r, eval=TRUE,message=FALSE,warning=FALSE,echo=FALSE}

seqstatd(sequence)[[1]]
```

---

```{r}
seqdplot(sequence, title="Distribution des états à chaque temps")

```
---
background-image: url(images/rawpixel/lion-jpeg.jpg)
background-size: contain
background-position: right
class: middle, center

.pull-left[
Quelques problèmes avec les  séquences :
Ce à quoi vous devez penser avant de débuter votre analyse

]
---
# To do list!

- Quel est mon alphabet ?

- Quelle est ma période d'étude ?

- Des valeurs manquantes sont-elles présentes dans mon jeu de données 

## Quel alphabet

C'est une question importante en terme de probl?me de codage

Exemple : Etude du statut marital 

- Seul, marié
- Seul, marié, divorcé, veuf



Attention : Plus on dispose d'états dans l'alphabet, plus les résultats sont complexes à interpréter


Plus il y a d'états, plus il est important que l’échantillon permette que quelques individus connaissent tous ces états


---
# L'alphabet

L'origine des problèmes de codage peut provenir du fait qu'on étudie en fait 2 types de trajectoires, comme par exemple, la migration et le statut matrimonial

Etude des séquences d'états `Multichannel` 

Exemple : 

- Vivant seul ou en union : A ou U
- Vivant au Sénégal ou dans un autre pays : S ou O
- Votre alphabet est alors AS-US-AO-UO

Mais changer de AS à US ne représente qu'un changement de statut matrimonial

Ceci ne prend clairement en compte qu'une trajectoire matrimoniale

Exemple : Intérêt de mixer 2 séquences ou plus ?

---
## Longueur de la séquence

Quelle est la période que je souhaite étudier ?

- Si j'étudie la trajectoire entre l'âge de 15 et 30 ans : Y-a-t-il des personnes âgées de moins de 30 ans dans mon échantillon ?

- Est-ce intéressant de comparer une période d'observation de 10 ans à une autre de 30 ans ?

---
# Valeurs manquantes

Où ?

- Au début de la séquence, parce que toutes les séquences ne débutent pas au même point initial
- A la fin, parce que la période entière n'est pas observée pour tous les individus de l'échantillon
- Entre 2 états, dues à la non-éponse, des trous internes 

---
# Valeurs manquantes

Ce qui peut être fait :

- Supprimer
- Prendre en compte en tant que valeur manquante
- Créer un nouvel état
- Imputer
---

# Valeurs manquantes à gauche
Les valeurs manquantes peuvent apparaître avant le premier état connu : *-*-A-A-B-B


- Les éléments manquants sont supprimés, ainsi la séquence part de A et on décide que le point de départ de la période est le premier état valide

- Les éléments manquants sont considérés comme manquants, ainsi ne sont pas pris en compte pour les statistiques transversales : 
 + Exemple : 3 séquences *-*-A-A-B-B;A-A-A-A-A-A;B-B-B-B-B-B
 
 + Pour la distribution des états au temps 1, seules deux séquences seront utilisées pour le calcul et la distribution sera de 50% pour A et 50% pour B


- Les éléments manquants sont considérés comme un élément de l'alphabet : Non parti (NP),  la séquence est maintenant : NP-NP-A-A-B-B 

-Les valeurs manquantes à gauche sont dues à différents points initiaux mais ceci est rare parce que dans le cadre de nombreuses problématiques, le point initial est très clairement défini et est le même pour tous

---
# Valeurs manquantes à droite
Les valeurs manquantes apparaissent avant le premier état connu :  A-A-B-B-* - 

- Du fait de la non-observation après le temps t (peut être consideré comme censurer à droite)
- Mêmes solutions
- En pratique, les valeurs manquantes sont considérées comme valeurs manquantes ou supprimées si et seulement si la période des données manquantes n'est pas trop importante comparée à la séquence entière

---
# valeurs manquantes dans la séquence

Le plus souvent, les trous sont dus à la non réponse 
A-* -A-A-B-B

- Supprimer ces valeurs manquantes n'est pas une bonne solution

- Les solutions les plus utilisées sont : 

  + utiliser un nouvel élément (NR) ou imputation
  + Un nouvel élément (A-NR-A-A-B-B) permet de décrire la non-réponse s'il y a plusieurs éléments NR 
  + Une imputation, le plus souvent par le dernier état connu (A-A-A-A-B-B), permet d'étudier une donnée complète comme nous y sommes habitués. 
  
  L'imputation est intéressante avec peu de non-réponses

---
