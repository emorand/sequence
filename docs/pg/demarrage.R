
# title: "Guide de démarage rapide"
# subtitle: "Donnees et analyse"
# author: "Elisabeth Morand"
# date: "2021/04/19 (updated: `r Sys.Date()`)"
# 


## ---- message=FALSE, warning=FALSE,echo=FALSE---------
source("01_pg_creation.R")




#' 
#' # Aspect pratique
#' 
#' 1- Creer un dossier pour l'ensemble des fichiers qui vont être créés au cours de cette séance de formation
#' 
#' 2- Dans ce dossier creer un dossier Donnees . Ce dossier contiendra deux dossiers : "In", "Out"
#' 
#' 3- Creer un nouveau projet  Rstudio dans ce dossier 
#' 
#' ---
#' # Les données
#' Les données tabulaires utilisées ont été collectées par Jeffrey Lancaster et sont issues de [ce projet](https://jeffreylancaster.github.io/game-of-thrones/).  Pour débuter, on va utiliser les données d'[Etienne Come](https://github.com/comeetie/got/tree/master/data) 
#' 
#' Elles sont à disposition [ici](https://gitlab.huma-num.fr/emorand/sequence/-/tree/master/Donnees/In)
#' 
#' On a modifiée les données afin d'ajouter des variables utiles à l'analyse. Le programme est disponible [là](01_pg_creation.R)
#' 
#' Les données que l'on utilise [ici](https://gitlab.huma-num.fr/emorand/sequence/-/tree/master/Donnees/out)
#' 
#' ---
#' 
#' 
#' # Description des données
#' 
#' 
#' ---
#' # Un coup d'oeil

#' 
## ----message=FALSE, warning=FALSE---------------------
library(DataExplorer)

#' 
#' 
## ----message=FALSE, warning=FALSE---------------------
library(gt)
introduce(loc_got)%>%
  gt()

#' On a donc , `r nrow(loc_got)` episodes qui contiennent au maximum `r ncol(loc_got)-1 ` scenes. 
#' ]
#' 

#' 
#' 
## ---- out.width="60%",echo=FALSE----------------------
plot_intro(loc_got)


#' 
#' ---
#' # le contenu
## ---- results="asis"----------------------------------
loc_got%>%
  filter(episode_id<5)%>%
  gt()

#' 
#' 
#' ---
#' # Une sequence
#' 
#' Un episode est  une sequence de localisation.
#' Un episode est decoupé en scene , unité de temps, qui se déroule en un lieu.
#' 
#' Pour l'episode 1, on retrouve donc la séquence


library(TraMineR)
seq.localisation <- seqdef(loc_got, var = 2:ncol(loc_got))
print(seq.localisation[1,], format = "SPS")

#' 
#' Constat : l'épisode 1 se passe beaucoup au nord du mur, un peu au Nord marginalement au Pentos et une seule fois au Crownlands.


## Les lieux
#' Le "temps" passé dans chaque lieu
#' l'option prop=TRUE pour calculer un temps moyen ramené à la longueur de la séquence


library(ggplot2)
donnees<-seqmeant(seq.localisation,prop=TRUE)%>%
  as.data.frame()

donnees %>%
  mutate(lieu = fct_reorder(row.names(donnees), Mean)) %>%
  ggplot(aes(x=lieu, y=Mean)) +
  geom_point( color="blue", size=3) +
  theme_light() +
  coord_flip() +  theme(
    panel.border = element_blank(),
    axis.ticks.y = element_blank()
  )



#' Conclusion : en moyenne , 30 % des scenes d'un episode se passent dans le Crowland.

#' 
#' ---
#' # Dispersion
#' 
## ----echo=FALSE,message=FALSE, warning=FALSE----------
library(Hmisc)
#------ça je l'ai pris ici
#https://garthtarr.com/category/r/
donnees<-seqmeant(seq.localisation,prop=TRUE,serr=TRUE)%>%
  as.data.frame() 


  donnees$lower <- donnees$Mean-donnees$SE 
  donnees$upper <- donnees$Mean+donnees$SE 
  
  donnees$nom<-row.names(donnees)
  o<-order(donnees$Mean)
  
  donnees$nom <- fct_relevel(
  donnees$nom,donnees$nom[o]
  
)

Dotplot(nom ~ Cbind(Mean, lower, upper) , data=donnees) 


#' 
#' ---
#' # Des lieux absorbants 
#' 
## ---- echo=FALSE,message=FALSE,warning=FALSE----------
seq.localisation2 <- seqdef(loc_got2, var = 2:ncol(loc_got2))
 tr <- seqtrate(seq.localisation2)


#' 
## ----echo=FALSE, warning=FALSE,message=FALSE,out.width='75%'----
 library(diagram)
dimo<-length(alphabet(seq.localisation2))
M<-matrix(0,dimo,dimo)
 diag(M) <- diag(tr)
 dimnames(M)<-dimnames(tr)


 plotmat(M,pos=c(rep(2,3),1),
               lwd = 1, box.lwd = 2, cex.txt = 0.8, box.type = 'none', box.prop = 0.2,
               main = "plotmat", self.cex = 0.5,self.shiftx = c(-0.1, 0.1, -0.1, 0.1),curve=0)
 

#' 
#' ---
#' # L'impact des saisons
#' 

## ----oma1b,echo=FALSE, warning=FALSE, message=FALSE,out.width='60%'----
matrix_cout <- seqsubm(seq.localisation, method = "CONSTANT", cval = 2)

dist_seq<- seqdist(seq.localisation, method = "OM", sm = matrix_cout )
pmcar <-cmdscale(as.dist(dist_seq),k=4,eig=TRUE,x.ret=TRUE)

# On creer les points à representer:
representation<-data.frame(episodeId=loc_got$episode_id,pmcar$points)
representation<-merge(representation,episodes, by="episodeId")%>%
  select(episodeId,X1,X2,X3,X4,episodeNum,seasonNum)%>%
  mutate(identif=paste(seasonNum,episodeNum,sep='.'))

 plot(representation$X1,representation$X2,type="p",cex=0.3,asp=1)
 text(representation$X1,representation$X2,label=representation$identif)


#'  Les episodes 1,2,3,4 de la saison 8 se passent quasiment exclusivement dans the North
#'  
## ----echo=FALSE---------------------------------------
 print(seq.localisation[68:71,],format = "SPS")



#' 
#' 
#' ---
#' # Representation plus complète
#' 
#' 
## ----echo=FALSE,out.width='50%'-----------------------
op<-par(mfrow=c(1,2))
plot(representation$X3,representation$X4,type="p",cex=0.3,asp=1)
text(representation$X3,representation$X4,label=representation$identif)
 plot(representation$X1,representation$X2,type="p",cex=0.3,asp=1)
 text(representation$X1,representation$X2,label=representation$identif)
par(op)

#' 
#' 
#' 
#' ---
#' # Des saisons et des lieux
#' 
#' Limité au 7 premières saisons 
#' 
## ---- echo=FALSE, message=FALSE-----------------------
matrix_cout <- seqsubm(seq.localisation[-c(68:73),], method = "CONSTANT", cval = 2)

dist_seq<- seqdist(seq.localisation[-c(68:73),], method = "OM", sm = matrix_cout )

library(cluster)
classification_ward<- agnes(dist_seq, diss = TRUE, method = "ward")

classification_ward_final<-as.factor(cutree(classification_ward,4))
representation2<-representation[-c(68:73),]
representation2$class<-classification_ward_final


#' 
#' 
#' ---
#' # Des saisons et des lieux
#' 
## ---- echo=FALSE, message=FALSE, warning=FALSE--------
representation2$seasonNum<-as.factor(representation2$seasonNum)
numpart<-which(colnames(representation2)=="class")

library(clv)

std <- std.ext(as.integer(representation2$class), as.integer(representation2$seasonNum))
rand1 <- clv.Rand(std)
cat("indice de rand entre saison et classification")
rand1

library(gtsummary)
representation2 %>%
  select(seasonNum,class)%>%
  tbl_summary(by = class)


#' ---
#' # On recommence!
#' 
#' - L'analyse de séquence : vocabulaire de départ [aka : préparation des données]
#' 
#' - Decrire des sequences [aka : descriprion univariée]
#' 
#' - Décrire des séquences : nouveaux indicateurs [aka : description bivariée]
#' 
#' - Décrire plusieurs séquences [aka analyse multiple, multivariée, factorielle..]
#' 
#' - Groupe de séquences [aka classification]
#' 
#' 
#' 
